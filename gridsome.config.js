/* Global preprocessor files */
const path = require('path');

function addStyleResource(rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        path.resolve(__dirname, './src/styles/setup/*.scss'),
      ],
    });
}

/* Gridsome config */
module.exports = {
  siteName: 'Cécile Fécherolle',
  siteDescription: 'Online résumé and portfolio of Cécile Fécherolle, full-stack JavaScript and Java/Spring web'
    + 'developer in Lyon (France).',
  titleTemplate: '%s - Cécile Fécherolle',
  plugins: [
    {
      use: 'gridsome-plugin-i18n',
      options: {
        locales: [
          'fr',
          'en',
        ],
        fallbackLocale: 'fr',
        defaultLocale: 'fr',
        rewriteDefaultLanguage: false,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './blog/**/*.md',
        typeName: 'ArticlePost',
        remark: {
          externalLinksTarget: '_blank',
          externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
          plugins: [
            ['gridsome-plugin-remark-prismjs-all', {
              highlightClassName: 'gridsome-highlight',
              codeTitleClassName: 'gridsome-code-title',
              classPrefix: 'language-',
              aliases: {},
              noInlineHighlight: false,
              showLineNumbers: false,
              languageExtensions: [],
              prompt: {
                user: 'root',
                host: 'localhost',
                global: false,
              },
            }],
          ],
        },
      },
    },
  ],
  chainWebpack(config) {
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];

    types.forEach((type) => {
      addStyleResource(config.module.rule('scss').oneOf(type));
    });
  },
};
