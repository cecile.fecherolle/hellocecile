import LanguageSelector from '~/components/LanguageSelector.vue';

export default {
  components: {
    LanguageSelector,
  },
  metaInfo() {
    return {
      title: this.$i18n.t('page_titles.index'),
      meta: [{
        description: this.$t('site_description'),
      }],
    };
  },
};
