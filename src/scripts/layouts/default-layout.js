import Footer from '~/components/Footer.vue';
import LanguageSelector from '~/components/LanguageSelector.vue';
import Logo from '~/components/Logo.vue';
import MainMenu from '~/components/MainMenu.vue';
import { MAIN_MENU_MODES } from '~/scripts/constants';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className += ' default-layout';
    }
  },
  components: {
    Footer,
    LanguageSelector,
    Logo,
    MainMenu,
  },
  computed: {
    currentRouteName() {
      // Parsing the current path to extract route name, paths should look like:
      // /[lang]/[routeName]/[...]
      // or
      // /[routeName]/[...]
      const pathParts = this.$router.currentRoute.path.split('/');
      if (pathParts[1] && pathParts[1].length === 2) {
        // There is a language prefix
        return pathParts[2];
      }
      return pathParts[1];
    },
    pageTitle() {
      if (this.currentRouteName) {
        return this.$i18n.t(`page_titles.${this.currentRouteName}`).includes('page_titles.')
          ? this.$i18n.t('page_titles.not_found')
          : this.$i18n.t(`page_titles.${this.currentRouteName}`);
      }
      return this.$i18n.t('page_titles.not_found');
    },
  },
  data() {
    return {
      scrollPosition: 0,
      scrollForColorChange: null,
      MAIN_MENU_MODES,
    };
  },
  props: {
    blogPostLangRef: {
      type: String,
      required: false,
    },
    blogPostPageTitle: {
      type: String,
      required: false,
    },
  },
  metaInfo() {
    return {
      title: this.blogPostPageTitle || this.pageTitle,
      meta: [{
        description: this.$t('site_description'),
      }],
    };
  },
  methods: {
    updateScroll() {
      this.scrollPosition = window.scrollY;
    },
  },
  mounted() {
    window.addEventListener('scroll', this.updateScroll);

    const introBlockElement = document.querySelector('.intro-block');
    this.scrollForColorChange = introBlockElement ? introBlockElement.clientHeight : 0;
  },
};
