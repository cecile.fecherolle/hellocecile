export default {
  props: {
    placeholder: {
      type: String,
      required: true,
    },
    'page-title': {
      type: String,
      required: false,
    },
  },
};
