import customSanitizer from '~/scripts/mixins/custom-sanitizer';

export default {
  mixins: [customSanitizer],
  props: {
    content: {
      type: String,
      default: '',
    },
  },
};
