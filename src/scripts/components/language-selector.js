import CountryFlag from 'vue-country-flag';
import { AVAILABLE_LOCALES, CUSTOM_EVENTS, MAIN_MENU_MODES } from '~/scripts/constants';
import { GlobalEventBus } from '~/scripts/eventBus';


export default {
  components: {
    CountryFlag,
  },
  data() {
    return {
      AVAILABLE_LOCALES,
      MAIN_MENU_MODES,
    };
  },
  props: {
    blogPostLangRef: {
      type: String,
      required: false,
    },
    type: {
      type: String,
      required: false,
    },
  },
  methods: {
    changeLocale(newLocale) {
      this.$i18n.locale = newLocale;
      let newPath = this.$route.path;

      if (this.blogPostLangRef) {
        // We are currently reading a blog article, so the switch must change the site locale but also try to browse to
        // the corresponding article in the new locale
        newPath = `/blog/article/${this.blogPostLangRef}-${newLocale}/`;
      }

      this.$router.push({
        path: this.$tp(newPath, newLocale, true),
      });
    },
  },
  mounted() {
    if (this.type === MAIN_MENU_MODES.FULL) {
      GlobalEventBus.$on(CUSTOM_EVENTS.LANGUAGE_SWITCH_FROM_BLOG_POST, (newLocale) => {
        this.changeLocale(newLocale);
      });
    }
  },
  destroyed() {
    GlobalEventBus.$off(CUSTOM_EVENTS.LANGUAGE_SWITCH_FROM_BLOG_POST);
  },
};
