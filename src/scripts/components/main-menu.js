import { mixin as clickaway } from 'vue-clickaway';
import { Vue } from 'gridsome/app';
import { MAIN_MENU_MODES } from '~/scripts/constants';

export default {
  data() {
    return {
      pageList: ['home', 'projects', 'skills', 'contact', 'blog', 'about'],
      menuBarsIcon: require('~/assets/svg/menu-bars-icon.svg'),
      menuCloseIcon: require('~/assets/svg/menu-close-icon.svg'),
      isBurgerMenuExpanded: false,
      headerElementHeight: 0,
      debouncedResizeCallback: this._.debounce(this.handleResize, 300),
      MAIN_MENU_MODES,
    };
  },
  methods: {
    handleResize() {
      this.updateHeaderHeight();

      if (this.isBurgerMenuExpanded) {
        this.repositionBurgerMenu();
      }
    },
    hideBurgerMenu() {
      const burgerMenuLinksElement = document.querySelector('#burger-menu-links');
      if (burgerMenuLinksElement) {
        burgerMenuLinksElement.style.maxHeight = 0;
      }
      this.isBurgerMenuExpanded = false;
    },
    repositionBurgerMenu() {
      Vue.nextTick(() => {
        const burgerMenuLinksElement = document.querySelector('#burger-menu-links');
        if (burgerMenuLinksElement) {
          burgerMenuLinksElement.style.top = `${this.headerElementHeight}px`;

          let totalChildrenHeight = 0;
          const burgerMenuChildren = burgerMenuLinksElement.children;
          for (let i = 0; i < burgerMenuChildren.length; i++) {
            totalChildrenHeight += burgerMenuChildren[i].clientHeight;
          }

          if (this.isBurgerMenuExpanded) {
            burgerMenuLinksElement.style.maxHeight = `${totalChildrenHeight}px`;
          }
        }
      });
    },
    toggleBurgerMenu() {
      this.isBurgerMenuExpanded = !this.isBurgerMenuExpanded;

      if (this.isBurgerMenuExpanded) {
        this.updateHeaderHeight();
        this.repositionBurgerMenu();
      } else {
        this.hideBurgerMenu();
      }
    },
    updateHeaderHeight() {
      const headerElement = document.querySelector('header');
      this.headerElementHeight = headerElement.clientHeight;
    },
  },
  mixins: [clickaway],
  computed: {
    isReduced() {
      return this.type === MAIN_MENU_MODES.REDUCED;
    },
  },
  mounted() {
    this.updateHeaderHeight();

    if (this.isReduced) {
      this.hideBurgerMenu();
      window.addEventListener('resize', this.debouncedResizeCallback);
    }
  },
  props: {
    type: {
      type: String,
      required: false,
    },
  },
  beforeDestroy() {
    if (this.isReduced) {
      window.removeEventListener('resize', this.debouncedResizeCallback);
    }
  },
};
