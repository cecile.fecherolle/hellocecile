export default {
  data() {
    return {
      pagoda: require('~/assets/svg/pagoda.svg'),
      logoName: require('~/assets/svg/logo-name.svg'),
    };
  },
};
