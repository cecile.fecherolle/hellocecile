export default {
  data() {
    return {
      linkedInLogo: require('~/assets/svg/linkedin-logo.svg'),
      currentYear: new Date().getFullYear(),
    };
  },
};
