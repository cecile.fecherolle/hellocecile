export const AVAILABLE_LOCALES = [
  { flag: 'fr', code: 'fr', name: 'french' },
  { flag: 'en', code: 'gb', name: 'english' },
];
export const CUSTOM_EVENTS = {
  LANGUAGE_SWITCH_FROM_BLOG_POST: 'LANGUAGE_SWITCH_FROM_BLOG_POST',
};
export const HYVOR_TALK = {
  WEBSITE_ID: 1092,
};
export const MAIN_MENU_MODES = {
  REDUCED: 'reduced',
  FULL: 'full',
};
