const sanitizeHtml = require('sanitize-html');

export default {
  methods: {
    customSanitizeHtml: (dirty) => sanitizeHtml(dirty, {
      allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img', 'h1', 'h2']),
    }),
  },
};
