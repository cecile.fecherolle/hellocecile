export default {
  methods: {
    formatISODateTimeToLocale: (dateTime, locale) => new Date(dateTime).toLocaleDateString(locale,
      {
        day: 'numeric',
        month: 'short',
        year: 'numeric',
      }),
  },
};
