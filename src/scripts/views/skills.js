import IntroBlock from '~/components/IntroBlock.vue';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className = 'skills';
    }
  },
  components: {
    IntroBlock,
  },
  data() {
    return {
      placeholder: require('~/assets/svg/bonsai.svg'),
    };
  },
};
