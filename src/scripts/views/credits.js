import IntroBlock from '~/components/IntroBlock.vue';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className = 'credits';
    }
  },
  components: {
    IntroBlock,
  },
  data() {
    return {
      placeholder: require('~/assets/svg/maneki-neko.svg'),
    };
  },
};
