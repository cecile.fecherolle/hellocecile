import IntroBlock from '~/components/IntroBlock.vue';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className = 'home';
    }
  },
  components: {
    IntroBlock,
  },
  data() {
    return {
      placeholder: require('~/assets/svg/koinobori.svg'),
      bicolorOrigamiBirds: require('~/assets/svg/bicolor-origami-birds.svg'),
      mountFuji: require('~/assets/svg/mount-fuji.svg'),
      tea: require('~/assets/svg/tea.svg'),
      origamiIcon: require('~/assets/svg/origami-icon.svg'),
    };
  },
};
