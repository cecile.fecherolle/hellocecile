import IntroBlock from '~/components/IntroBlock.vue';
import dateFormatter from '~/scripts/mixins/date-formatter';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className = 'blog';
    }
  },
  components: {
    IntroBlock,
  },
  mixins: [dateFormatter],
  data() {
    return {
      placeholder: require('~/assets/svg/wagasa.svg'),
    };
  },
  methods: {
    getArticlesForCurrentLocale() {
      return this.$page ? this.$page.posts.edges.filter((edge) => edge.node.lang === this.$i18n.locale) : [];
    },
  },
};
