import IntroBlock from '~/components/IntroBlock.vue';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className = 'projects';
    }
  },
  components: {
    IntroBlock,
  },
  data() {
    return {
      placeholder: require('~/assets/svg/bamboo.svg'),
    };
  },
};
