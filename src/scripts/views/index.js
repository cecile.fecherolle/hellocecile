import IndexLayout from '~/layouts/IndexLayout.vue';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className = 'index';
    }
  },
  components: {
    IndexLayout,
  },
  data() {
    return {
      blueFish: require('~/assets/svg/bottom-left-blue-fish.svg'),
      redFish: require('~/assets/svg/top-right-red-fish.svg'),
      foliage1: require('~/assets/svg/foliage-1.svg'),
      foliage2: require('~/assets/svg/foliage-2.svg'),
    };
  },
};
