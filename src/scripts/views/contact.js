import IntroBlock from '~/components/IntroBlock.vue';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className = 'contact';
    }
  },
  components: {
    IntroBlock,
  },
  data() {
    return {
      placeholder: require('~/assets/svg/origami-boat.svg'),
      errors: [],
      name: null,
      email: null,
      message: null,
    };
  },
  methods: {
    checkForm(e) {
      this.errors = [];

      if (!this.name) {
        this.errors.push(this.$t('contact.name_required'));
      }
      if (!this.email) {
        this.errors.push(this.$t('contact.email_required'));
      } else if (!this.validEmail(this.email)) {
        this.errors.push(this.$t('contact.email_valid'));
      }

      if (!this.message) {
        this.errors.push(this.$t('contact.message_required'));
      }

      if (!this.errors.length) {
        return true;
      }

      e.preventDefault();
      return false;
    },
    validEmail(email) {
      // eslint-disable-next-line max-len
      const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return emailRegex.test(email);
    },
  },
  beforeRouteLeave(to, from, next) {
    // Empty error messages in case user switched site language: avoid displaying errors in previous language
    this.errors = [];
    next();
  },
};
