const messageStyling = 'color:teal;font-size:1.2rem;font-weight:bold';

console.log(`%c                                                                                
                           M@WM                                                 
                             @0ZZ0BW@@@W@M                                      
                                @0aXr;rrrXZM                                    
                                    BS7rrrrS@                                   
                      @00B@@W0ZZZ8@   MW827rXW    MM                            
                   M0X7rrrrrrrrrrrraZa2a8B@0S70   MZXaBM                        
                   @WMMMW0ZS7rrrrrr;;;rrrr7SZZXa@   WS;rSZZ0M                   
                            MW0aXrr;;;rrrrr7XXXX7SZB@MWa7777X2Z8WM              
        M@WBBM                   MW0000BWW@M   MMW0aXX2a2XXXXXXSSaBM            
       @B888Z0M                                     MBaXXXXXXSSS2228M           
     MW00888ZZM                                        WZXXXSS22aa22aM      MM  
    MW00088ZZ8M                                  MM     MW2XXXS2aaaa2W      @8M 
    @B00088SZB                                   @8BM     M8XXSSSaZa2ZM     W28M
   MBB000888ZW                                    @ZZWM     W2SSSS222SZ@   Ma2a@
   @BB000888Z@                                     @ZaZBM    @aSSSS22SSaBM BS2a0
   WBB000888ZWWWM                                   @aaaa8@   M2SSSS2SXX20WSS2aZ
  MBZ0000888Z80080@M                                MZaaaaZWMM @SSSSS2S7X2aXS2aa
  B8ZX000888Z8008ZZ8WM                               WaaaaaaaaZ00SSSS22XrrX2S2aa
MW2Z0rX80888ZZ808ZZZaa80WM                           MBaaaa222aaa2SSS222X;7a22aB
BZ2S07rX8888ZZZ08ZZZa2SSXZM                            MWBZaa2aa2aSSS2222Xraaa@ 
0Z2X2a7rX888ZZZBWB8Za2SSXX0                                @0aaa2a2SS22222XSaW  
0ZaX780XXS88ZZZZW  M@ZSSXXSM                                 MBZ22aSS2222222ZM  
0ZaSSW@8SS2ZZZZZZ@   M02XX7W                                   MW@BSS222222a8   
W8aSW  @022ZZZZZaZ@    MWZ7XW                                     WSS222222aB   
M8aZM   MBaZZ8ZZaaaBM     WS70                                    WS2222222ZM   
 BaW     M0Z800ZaaaaZ@     MB2B                                   8S2222222W    
 M0@      WZ88008Zaa2aBM                                         MaSS222220M    
          M0ZZ88ZZaa222aBM                                       MSSS222aBM     
           MBZZZZa22SXSSS20@M                                    M8SS228@       
             @8222SSXXXa8ZaSSZ0W@MM@WB08ZaaaaZBM                  MB0WM         
               MB0ZSX77rS0MM@0aX77rr;;;;iiii:::i728WM                           
                   MW08aXrX0M  M8XaZ27;;iiii::::::::i728BWW8@                   
                        MW82aM   WX7aW@B8aZ0Z;::::::i:,,,:XB                    
                             M    @X;;720@M  MB080BM M@WWM                      
                                   M2;;;ii;8M                                   
                                    M0X;;;;;;7aBM                               
                                       MMMMMM@B8a8WM                            
                                                  @@M      `,
'color:OrangeRed;');
console.log('%cHi there! Are you curious about my code? Since this is a generated static website, I\'m afraid you won\'t find much useful info in the dev tools.',
  messageStyling);
console.log('%cIf you want to see what\'s what, you should check the Git repository: https://gitlab.com/cecile.fecherolle/hellocecile',
  messageStyling);
