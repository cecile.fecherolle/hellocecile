/* global hyvor_talk */
/* eslint-disable no-undef */
import CountryFlag from 'vue-country-flag';
import ArticleContent from '~/components/ArticleContent.vue';
import { AVAILABLE_LOCALES, CUSTOM_EVENTS, HYVOR_TALK } from '~/scripts/constants';
import { GlobalEventBus } from '~/scripts/eventBus';
import dateFormatter from '~/scripts/mixins/date-formatter';
import customSanitizer from '~/scripts/mixins/custom-sanitizer';

export default {
  beforeCreate() {
    if (process.isClient) {
      document.documentElement.className = 'article-post';
    }
  },
  components: {
    ArticleContent,
    CountryFlag,
  },
  mixins: [dateFormatter],
  data() {
    return {
      shouldDisplayComments: false,
    };
  },
  metaInfo() {
    return {
      title: this.$page.post.title,
    };
  },
  watch: {
    $page(newValue) {
      this.updateHyvorTalkConfig(newValue.post.id);
      hyvor_talk.reload();
      document.querySelector('#load-comments-button').style.display = 'block';
    },
  },
  mounted() {
    // Define Hyvor Talk config for current blog post
    HYVOR_TALK_WEBSITE = HYVOR_TALK.WEBSITE_ID;
    this.updateHyvorTalkConfig(this.$page.post.id);

    // Inject Hyvor talk external script to load comments interface
    const hyvorTalkScript = document.createElement('script');
    hyvorTalkScript.setAttribute('async', '');
    hyvorTalkScript.setAttribute('src', 'https://talk.hyvor.com/web-api/embed');
    document.head.appendChild(hyvorTalkScript);

    // Add HTML comment if needed for Forestry preview mode
    const forestryInstantPreviewId = this.$page.post.forestry_instant_preview_id;
    if (forestryInstantPreviewId) {
      const htmlComment = document.createComment(
        customSanitizer.methods.customSanitizeHtml(`FORESTRY INSTANT PREVIEW ID: ${forestryInstantPreviewId} `),
      );
      document.body.appendChild(htmlComment);
    }
  },
  computed: {
    otherLocales() {
      return this.$static.allPosts.edges
        .filter(
          (post) => post.node.lang_ref === this.$page.post.lang_ref && post.node.lang !== this.$i18n.locale,
        )
        .map(
          (otherLocale) => AVAILABLE_LOCALES.find((availableLocale) => availableLocale.flag === otherLocale.node.lang),
        );
    },
  },
  beforeRouteLeave(to, from, next) {
    this.shouldDisplayComments = false;
    next();
  },
  methods: {
    updateHyvorTalkConfig(postId) {
      HYVOR_TALK_CONFIG = {
        url: window.location.href.replace(/\/$/, ''), // Remove trailing slash to avoid duplicate URLs/comment boards
        id: postId,
        loadMode: 'click',
        clickId: 'load-comments-button',
      };
    },
    switchToArticleInOtherLocale(otherLocale) {
      GlobalEventBus.$emit(CUSTOM_EVENTS.LANGUAGE_SWITCH_FROM_BLOG_POST, otherLocale);
    },
    transformImageUrl(pathToImage) {
      return pathToImage ? pathToImage.replace('/static', '') : pathToImage;
    },
  },
};
