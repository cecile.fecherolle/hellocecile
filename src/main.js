import Buefy from 'buefy';
import VueLodash from 'vue-lodash';
import lodash from 'lodash';
import 'buefy/dist/buefy.css';

import DefaultLayout from '~/layouts/DefaultLayout.vue';
import '~/styles/global.scss';

import '~/scripts/extra/easter-egg';

// Syntax highlighting
require('gridsome-plugin-remark-prismjs-all/themes/night-owl.css');
require('prismjs/plugins/command-line/prism-command-line.css');
require('prismjs/plugins/line-numbers/prism-line-numbers.css');

export default function (Vue, { appOptions }) {
  Vue.use(Buefy);
  Vue.use(VueLodash, { name: 'custom', lodash });

  Vue.component('DefaultLayout', DefaultLayout);

  appOptions.i18n.setLocaleMessage('fr', require('~/locales/fr.json'));
  appOptions.i18n.setLocaleMessage('en', require('~/locales/en.json'));
}
