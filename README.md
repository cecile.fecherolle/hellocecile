# About

This repository contains my personal website and portfolio, which I developed using a JAMStack architecture.

It is hosted and available [here](https://cecile.dev)!

This project was put together in different phases: technical decisions, mockups design, and then development.

I built the site on the following stack:
* Adobe XD, mockups design and graphic research
* VueJS, modern JavaScript framework
* Gridsome, VueJS framework which facilitates the generation of fast and efficient static websites
* CSS/SCSS, stylesheets handling and associated preprocessor
* Flexbox, design of unidimensional layouts
* Forestry.io, Git-based static CMS (for the technical blog)
* Hyvor Talk, comment service for blog pages
* Git, code versioning
* GitLab, hosting and handling of the Git repository
* Netlify, site hosting and automated deployment using continuous integration
* Yarn, NPM packages and dependencies handling tool
* ESLint/Stylelint, standardization and code quality tools for JavaScript and CSS
    
# Feedback

Constructive feedback is always welcome (and appreciated)!

In case you have trouble navigating the website because of a bug or any kind of issue, I would be glad to hear about it in the Issues section of this repository. For non-technical feedback or contact, please use the contact form in the website. :)
