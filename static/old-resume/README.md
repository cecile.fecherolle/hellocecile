# Fair warning

This folder contains the page and assets (scripts, images) of my old résumé, which I haven't touched since it was created somewhere around 2014, except for some cleanup of unused files when I put it in this repository.
 
 Please bear with me, as I just copied everything as it was and haven't refactored anything since.
 
 It is meant for displaying the page design I worked on back then, and is (clearly) not to brag about the code I wrote at that time.
 
 The page layout is based on the Immaculate template made by [Pritesh Gupta](https://www.priteshgupta.com/).

